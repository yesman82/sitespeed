var fs = require('fs'),
	path = require('path'),
	os = require('os');

module.exports = function(grunt) {
	// load grunt modules
	grunt.loadNpmTasks('grunt-sitespeedio');
	grunt.loadNpmTasks('grunt-env');
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-contrib-symlink');

	var args = process.argv;

	// default settings
	var options = {
		url: 'http://www.toptarif.de',
		file: null,
		deep: 0,
		no: 5,
		browser: 'chrome',
		connection: 'cable',
		phantomjsPath: path.normalize(process.cwd() + '/phantomjs'),
		resultBaseDir: './sitespeed-result',
		storeJson: true,
		requestHeaders: null,
		budget: null,
		graphiteHost: null
	};

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sitespeedio: {
			default: {
				options: options
			}
		},
		shell: {
			xvfb: {
				command: function() {
					// need to put in function
					return 'xvfb-run -s "-ac -screen 0, 10x10x8" grunt sitespeed-xvfb -options=' + encodeURIComponent(JSON.stringify(options));
				}
			},
			rmSymlink: {
				command: 'rm chromedriver'
			}
		},
		env: {
			chromedriver: {
				options: {
					push: {
						PATH: {
							value: path.normalize(process.cwd() + '/node_modules/chromedriver/lib/chromedriver'),
							delimiter: ';'
						}
					}
				}
			}
		},
		symlink: {
			chromedriver: {
				src: path.normalize(process.cwd() + '/node_modules/chromedriver/bin/chromedriver'),
				dest: 'chromedriver',
				options: {
					overwrite: true
				}
			}
		}
	});

	// url file overrules url
	if (options.file) {
	    options.url = null;
	}

	// check all arguments
	grunt.registerTask('getArgs', getSitespeedArgs);

	// get latest values
	grunt.registerTask('result', getResults);

	grunt.registerTask('sitespeed', runSitespeed);

	// run sitespeedio with options arg from shell:xvfb
	grunt.registerTask('sitespeed-xvfb', sitespeedXvfb);

	// Default task(s).
	grunt.registerTask('default', ['getArgs', 'sitespeed', 'result']);



	//////////////////////////////////////////
	/// Helper functions
	//////////////////////////////////////////

	/**
	 * Get arguments for sitespeed.io
	 */
	function getSitespeedArgs() {
		var helpMsg =
			'\n' +
			'Usage:\n' +
			'  grunt [options]\n' +
			'\n' +
			'Options:\n' +
			'  -?                           # for this page\n' +
			'  -url=http://www.example.com  # URL to test on (default: ' + options.url + ')\n' +
			'  -file=[path]                 # Give a bunch of URLs to test. Plain text file with one URL per row.\n' +
			'  -browser=[browser]           # Browser to use: headless, chrome (default: ' + options.browser + ')\n' +
			'  -runs=[number of runs]       # sets the number of runs to calculate average values from (default: ' + options.no + ')\n' +
			'  -depth=[depth]               # number of levels to dig (default: ' + options.deep + ')\n' +
			'  -connection=[type]           # Simulated connection types: mobile3g, mobile3gfast, cable, native (default: ' + options.connection + ')\n' +
			'  -resultBaseDir=[path]        # Directory where results are saved to (default: ' + options.resultBaseDir + ')\n' +
			'  -headers=[path]              # Add header values to URL call. Must be a JSON file.\n' +
			'  -budget=[path]               # Set budgets. Must be a JSON file.\n' +
			'  -graphiteHost=[IP or domain] # Sets the IP or domain for a graphite server.\n' +
			'\n' +
			'Use "grunt result" to just get last result';

		// check arguments for settings
		args.forEach(function(arg, i) {
			if (arg === '-?') {
				grunt.log.writeln(helpMsg);
				process.exit(0);
			}

			// check if value given
			if ((val = arg.substr(arg.indexOf('=') + 1)).length > 0) {
				if (arg.indexOf('-url=') === 0) { // get URL to test on
					options.url = val;
				} else if (arg.indexOf('-file=') === 0) {
				    options.file = path.normalize(val);
				} else if (arg.indexOf('-browser=') === 0) { // check for browser argument
					options.browser = val.toLowerCase();
				} else if (arg.indexOf('-runs=') === 0) { // check how many runs shall be done
					options.no = val;
				} else if (arg.indexOf('-depth=') === 0) { // check for depth argument
					options.deep = val;
				} else if (arg.indexOf('-connection=') === 0) { // check for throtteling argument
					options.connection = val.toLowerCase();
				} else if (arg.indexOf('-resultBaseDir=') === 0) { // check for result base directoy argument
					options.resultBaseDir = path.normalize(val);
				} else if (arg.indexOf('-headers=') === 0) { // check for header JSON file argument
					options.requestHeaders = JSON.parse(fs.readFileSync(path.normalize(val)));
				} else if (arg.indexOf('-budget=') === 0) { // check for header JSON file argument
					options.budget = JSON.parse(fs.readFileSync(path.normalize(val)));
				} else if (arg.indexOf('-graphiteHost=') === 0) { // check for header JSON file argument
					options.graphiteHost = val;
				}
			} // val check
		});

		if (options.file) {
		    options.url = null;
		}

		// url is essential
		if (!options.url && !options.file) {
			grunt.fail.fatal('Missing URL!\n' + helpMsg);
		}

		grunt.log.subhead('=== Settings ===');
		grunt.log.writeln(getKeyValueTable(options));
		grunt.log.writeln('----------------');
		grunt.log.writeln('Use -? for help');
	}

	/**
	 * Deceides whether to run sitespeed with xvfb or not
	 */
	function runSitespeed() {
		if (os.platform() === 'linux' && options.browser === 'chrome') {
			grunt.task.run('symlink:chromedriver', 'shell:xvfb', 'shell:rmSymlink');
			return;
		}

		grunt.task.run('env:chromedriver', 'sitespeedio');
	}

	/**
	 * Should be executed when grunt is run within xvfb container to adopt options and run sitespeed.
	 */
	function sitespeedXvfb() {
		args.forEach(function(arg) {
			if ((val = arg.substr(arg.indexOf('=') + 1)).length > 0 && arg.indexOf('-options=') === 0) {
				val = decodeURIComponent(val);
				try {
					options = JSON.parse(val);
				} catch (err) {
					grunt.fail.fatal('Couldn\'t parse options argument: ' + err);
				}
				// need to merge config
				grunt.config.merge({
					sitespeedio: {
						default: {
							options: options
						}
					}
				});
			}
		});
		grunt.task.run('sitespeedio');
	}

	/**
	 * Present all results to console
	 */
	function getResults() {
		var urls = [],
			domains = [],
			helpMsg, argUrl, result;

		helpMsg =
			'Usage:\n' +
			'grunt result [options]\n' +
			'\n' +
			'Options:\n' +
			'  -url=[url or domain]   # url or domain to get results of.\n' +
			'  -file=[path]           # Give a bunch of URLs or domains. Plain text file with one URL/domain per row.';

		// check args for settings
		args.forEach(function(arg, i) {
			if (arg === '-?') {
				grunt.log.writeln(helpMsg);
				process.exit(0);
			}

			if ((val = arg.substr(arg.indexOf('=') + 1)).length > 0) {
				if (arg.indexOf('-url=') === 0) {
					urls.push(val);
				} else if (arg.indexOf('-file=') === 0) {
					urls = fs.readFileSync(val, 'utf8').split('\n');
				}
			}
		});

		// ovverride if options has settings
		if (options && options.file) {
			urls = fs.readFileSync(val, 'utf8').split('\n');
		} else if (options && options.url) {
			urls.push(options.url);
		}

		urls.forEach(function(domain) {
			// build domain path from given url
			domain = domain.indexOf('://') > -1 ? domain.substring(domain.indexOf('://') + 3) : domain;
			domain = domain.indexOf('/') > -1 ? domain.substring(0, domain.indexOf('/')) : domain;

			if (domains.indexOf(domain) === -1) {
			    domains.push(domain);
			}
		});

		domains.forEach(function(domain) {
			result = getResultJson(domain);

			grunt.log.subhead('#### ' + domain + ' ####');
			grunt.log.subhead('=== Config ===');
			grunt.log.writeln(getKeyValueTable(result.config, Object.keys(options)));

			grunt.log.subhead('=== Aggregates ===');
			grunt.log.writeln(getAggregatesTable(result));
		});
	}

	/**
	 * Get result data from last test run
	 * @param  {String} domain Domain of what to get data from
	 * @return {JSON}          JSON holding result
	 */
	function getResultJson(domain) {
		var domainPath, dirs, lastDir, resultPath, result;
		domainPath = options.resultBaseDir + '/' + domain;

		// check if folder exists for domain
		if (!fs.existsSync(domainPath)) {
			grunt.fail.fatal('No results yet for domain ' + domain + '. Cannot find folder "' + domainPath + '"');
		}

		// get result  file from last created folder
		dirs = fs.readdirSync(domainPath);

		// check if any test result folders exist
		if (dirs.length < 1) {
			grunt.fail.fatal('No results yet for domain ' + domain + '.');
		}

		// get last bit of result file path
		lastDir = dirs[dirs.length - 1];
		resultPath = path.join(domainPath, lastDir, '/data/result.json');

		// check if result file exists
		if (!fs.existsSync(resultPath)) {
			grunt.fail.fatal('No result file found at "' + resultPath + '"');
		}

		// read file
		result = fs.readFileSync(resultPath, 'utf8');

		// parse JSON
		try {
			result = JSON.parse(result);
		} catch (err) {
			grunt.fail.fatal('Cannot parse JSON from file "' + resultPath + '"\nParsing Error: ' + err);
		}

		return result;
	}

	/**
	 * Returns a two-column table with given keys and their value.
	 * @param  {JSON/Object} data      Object holding the data.
	 * @param  {Array}       keysToGet An array of direct keys under the given data object.
	 * @return {String}                A grunt table
	 */
	function getKeyValueTable(data, keysToGet) {
		var str = '',
			lines = [],
			padding = 3,
			widths = [0, 0],
			value, keyWidth, valueWidth;

		if (!keysToGet) {
		    keysToGet = Object.keys(data);
		}

		keysToGet.forEach(function(key) {
			value = typeof data[key] === 'object' ? JSON.stringify(data[key]) : data[key] + '';
			valueWidth = value.length + padding;
			keyWidth = key.length + padding;
			widths[0] = keyWidth > widths[0] ? keyWidth : widths[0];
			widths[1] = valueWidth > widths[1] ? valueWidth : widths[1];
			lines.push([key, value]);
		});

		lines.forEach(function(line, i) {
			str += grunt.log.table(widths, line) + (i === lines.length - 1 ? '' : '\n');
		});

		return str;
	}

	/**
	 * Gets all sitespeed aggregates and puts them in a grunt table.
	 * @param  {JSON}   result The result file json
	 * @return {String}        A grunt table string
	 */
	function getAggregatesTable(result) {
		var widths = [],
			lines = [],
			padding = 3,
			str = '',
			text, line, widthPos;

		result.aggregates.forEach(function(aggregate, i) {
			line = [];

			if (typeof widths[0] === 'undefined') {
				widths.push(0);
			}

			text = aggregate.id;
			if (text.length + padding > widths[0]) {
				widths[0] = text.length + padding;
			}
			line.push(text);

			// shows all values
			Object.keys(aggregate.stats).forEach(function(statKey, i) {
				widthPos = i + 1;
				if (typeof widths[widthPos] === 'undefined') {
					widths.push(0);
				}

				text = statKey + ': ' + aggregate.stats[statKey];
				if (text.length + padding > widths[widthPos]) {
					widths[widthPos] = text.length + padding;
				}

				line.push(text);
			});

			lines.push(line);
		});

		lines.forEach(function(line, i) {
			str += grunt.log.table(widths, line) + (i === lines.length - 1 ? '' : '\n');
		});

		return str;
	}
};